from utility import *
import inspect
import ast
import z3

def path_conditions(func):
    source_code = inspect.getsource(func)
    astTree = ast.parse(source_code)
    return collect_path_conditions(astTree)
