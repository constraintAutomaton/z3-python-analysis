from utility import *
from typing import List
import copy


def analyseConstraint(func, precondition: str = None, invariant: str = None, postcondtion: str = None):
    source_code = inspect.getsource(func)
    astTree = ast.parse(source_code)
    path_conditions, variables, variable_raw = collect_path_conditions(astTree)
    variable_statement = generate_variable_statement(variables)

    path_conditions = deleteDeadCode(path_conditions, variable_statement)

    if precondition != None:
        path_conditions = appendPrecondition(
            path_conditions, precondition)
    if invariant != None:
        path_conditions = appendInvariant(
            path_conditions, variable_raw, invariant)
    if postcondtion != None:
        path_conditions = appendPostCondition(
            path_conditions, variable_raw, postcondtion)
    isValid = True
    paths = []

    for path_condition in path_conditions:
        s = z3.Solver()
        statement = generate_z3_statement(variable_statement, path_condition)
        exec(statement)
        sat = s.check().__repr__()
        if sat != "sat":
            isValid = False
            paths.append("{};is not valid".format(path_condition))
        else:
            paths.append("{};is valid".format(path_condition))
    if isValid:
        return ("the constraint are satisfy", path_conditions)
    else:
        return ("the constraint are not satisfy\n{}".format(' '.join([str(item)+"\n\n" for item in paths])), path_conditions)


def appendPrecondition(conditions: List[List[str]], pre: str) -> List[List[str]]:
    resp = copy.deepcopy(conditions)
    for i, _ in enumerate(conditions):
        resp[i].insert(0, pre)
    return resp


def appendInvariant(conditions: List[List[str]], variables, inv: str) -> List[List[str]]:
    effectiveCond = copy.deepcopy(conditions)
    maxVariables = findMaxVariable(variables)
    inv = "({})".format(inv)
    for i, cond in enumerate(conditions):
        for k, v in maxVariables.items():
            if k in inv:
                for j in range(0, v+1):
                    var = None
                    if j == 0:
                        var = k
                    else:
                        var = "{}{}".format(k, j)
                    if var in ",".join(cond):
                        invCor = remplaceAllOccurence_raw(inv, k, var)
                        effectiveCond[i].append(invCor)
    return effectiveCond


def appendPostCondition(conditions: List[List[str]], variables, post: str) -> List[List[str]]:
    effectiveCond = copy.deepcopy(conditions)
    maxVariables = findMaxVariable(variables)
    post = "({})".format(post)
    for i, cond in enumerate(conditions):
        for k, v in maxVariables.items():
            if k in post:
                if v != 0:
                    lastVar = None
                    for j in range(0, v+1):
                        var = None
                        if j == 0:
                            var = k
                        else:
                            var = "{}{}".format(k, j)
                        if var in ",".join(cond):
                            lastVar = var
                    if lastVar != None:
                        localPost = remplaceAllOccurence_raw(
                            post, k, lastVar)  # to check
                        effectiveCond[i].append(localPost)
                else:
                    effectiveCond[i].append(post)
    return effectiveCond


def getInvariant(conditions: List[List[str]], variables, inv: str) -> List[List[str]]:
    invariants = []
    maxVariables = findMaxVariable(variables)
    inv = "({})".format(inv)
    for cond in conditions:
        invariants.append([])
        for k, v in maxVariables.items():
            if k in inv:
                for j in range(0, v+1):
                    var = None
                    if j == 0:
                        var = k
                    else:
                        var = "{}{}".format(k, j)
                    if var in ",".join(cond):
                        invCor = remplaceAllOccurence_raw(inv, k, var)
                        invariants[-1].append(invCor)
    return invariants
