
import inspect
import ast
import z3
import astor
from typing import List
from utility import *

DEADCODE = "DEADCODE"


def test_generator(func):
    source_code = inspect.getsource(func)
    astTree = ast.parse(source_code)
    path_conditions, variables, _ = collect_path_conditions(astTree)

    return test_generator_from_condition(path_conditions, variables, func),path_conditions


def test_generator_from_condition(path_conditions: List[List[str]], variables, func):
    variable_statement = generate_variable_statement(variables)
    testCase = []
    for path_condition in path_conditions:
        s = z3.Solver()
        statement = generate_z3_statement(variable_statement, path_condition)
        exec(statement)
        sat = s.check().__repr__()
        if sat == "sat":
            m = s.model()

            x = m[z3.Int("x")].as_long() if m[z3.Int("x")
                                              ] != None else float('inf')
            y = m[z3.Int("y")].as_long() if m[z3.Int("y")
                                              ] != None else float('inf')
            k = m[z3.Int("k")].as_long() if m[z3.Int("k")
                                              ] != None else float('inf')
            v = "value [x:{},y:{},k:{}]; response: {}".format(
                x,
                y,
                k,
                func(x, y, k)
            )
            testCase.append(v)
        else:
            testCase.append(DEADCODE)

    return testCase
