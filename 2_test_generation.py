from test_generator import *
from examples.test_generation.example_1 import *
from examples.test_generation.example_2 import *


test_case, path_condition = test_generator(example_1)
print("---{}---".format(example_1.__name__))
print("---path condition---")
for i,c in enumerate(path_condition):
    print("{}: {}".format(i,c))
print("---test generation---")
for i,t in enumerate(test_case):
    print("{}: {}".format(i,t))

test_case, path_condition = test_generator(example_2)
print("---{}---".format(example_2.__name__))
print("---path condition---")
for i,c in enumerate(path_condition):
    print("{}: {}".format(i,c))
print("---test generation---")
for i,t in enumerate(test_case):
    print("{}: {}".format(i,t))
