from examples.path_condition import example_1,example_2

from path_condition import *

condition,var,var_raw = path_conditions(example_1)

print("---path condition---")
for c in condition:
    print(c)
print("--variable---")
print(var)
print("--variable raw---")
print(var_raw)

condition,var,var_raw = path_conditions(example_2)

print("---path condition---")
for c in condition:
    print(c)
print("--variable---")
print(var)
print("--variable raw---")
print(var_raw)

