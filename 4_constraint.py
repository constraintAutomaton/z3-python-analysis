from precondition_invariant_postcondition import *
from examples.constraint.example_1 import *

sat, cond = analyseConstraint(example_1, precondition="x<3")
print("---{}---".format("precondition"))
print("---path condition with constraint---")
for i, c in enumerate(cond):
    print("{}: {}".format(i, c))
print("---sat---")
print(sat)

sat, cond = analyseConstraint(example_1, postcondtion="y==2")
print("---{}---".format("post condition"))
print("---path condition with constraint---")
for i, c in enumerate(cond):
    print("{}: {}".format(i, c))
print("---sat---")
print(sat)

sat, cond = analyseConstraint(example_1, invariant="y<=1")
print("---{}---".format("invariant"))
print("---path condition with constraint---")
for i, c in enumerate(cond):
    print("{}: {}".format(i, c))
print("---sat---")
print(sat)

sat, cond = analyseConstraint(
    example_1, precondition="x<3", invariant="k>2", postcondtion="y>1")
print("---{}---".format("all"))
print("---path condition with constraint---")
for i, c in enumerate(cond):
    print("{}: {}".format(i, c))
print("---sat---")
print(sat)
