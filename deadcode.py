
import inspect
import ast
import z3
import astor
from typing import List
from test_generator import *


def findDeadCode(func):
    testCases, conditions = test_generator(func)
    deadCondition = []
    for i, testCase in enumerate(testCases):
        if testCase == DEADCODE:
            deadCondition.append(conditions[i])
    return deadCondition