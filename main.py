from examples.example1 import *
from examples.example2 import *
from test_generator import *
from deadcode import *
import re
import utility
from precondition_invariant_postcondition import *
from equivalent import *
from secutity import *
functions = [
    example_1,
    # example_2,
]
print(liveness(example_1,"x>100"))
"""
for func in functions:

    testCase, condition = test_generator(func)
    dead_code = findDeadCode(func)
    respConstraint = analyseConstraint(
        func, precondition="y>-100", invariant="x>-100", postcondtion="k>100")
    print("----------------{}----------------".format(func.__name__))
    print("--{}--".format("condition"))
    for c in condition:
        print(c)
        print("\n")
    print("--{}--".format("test generation"))
    for t in testCase:
        print(t)
        print("\n")
    print("--{}--".format("dead code"))
    for d in dead_code:
        print(d)
        print("\n")
    print("--{}--".format("constraint analysis"))
    print(respConstraint)
    print("\n")
"""