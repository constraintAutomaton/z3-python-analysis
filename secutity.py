import inspect
import ast
import z3
import astor
from typing import List
from utility import *
from precondition_invariant_postcondition import *


def safety(func, cond: str):
    cond = "z3.Not({})".format(cond)
    source_code = inspect.getsource(func)
    astTree = ast.parse(source_code)
    path_conditions, variables, variable_raw = collect_path_conditions(astTree)
    variable_statement = generate_variable_statement(variables)
    is_valid = True
    invalidNode = []
    path_conditions = deleteDeadCode(path_conditions, variable_statement)
    effectiveCond = getInvariant(path_conditions, variable_raw, cond)
    for i, path_condition in enumerate(path_conditions):
        conditions = []
        j = 0
        conditions.append(effectiveCond[i][j])
        for c in path_condition:
            conditions.append(c)

            if cond[7] in c:
                if "==" in c:
                    j += 1
                    conditions.append(effectiveCond[i][j])

                s = z3.Solver()
                statement = generate_z3_statement(
                    variable_statement, conditions)
                exec(statement)
                sat = s.check().__repr__()
                if sat != "sat":
                    is_valid = False
                    invalidNode.append("{} in {}".format(
                        c, path_condition))
            if "==" in c:
                del conditions[-1]
    if not(is_valid):
        invalidNode = "\n".join(invalidNode)
        return "safety not satisfy; those node are not valid\n`{}`".format(invalidNode)
    return "{}".format(is_valid)
