from equivalent import *
from examples.equivalance.random import *
from examples.equivalance.b1 import *
from examples.equivalance.b2 import *
from examples.equivalance.s1 import *
from examples.equivalance.s2 import *

print("---couple {},{} similaire---".format(s1.__name__, s2.__name__))
resp = simlation(s1, s2)
print(resp)
print("\n")
print("---couple {},{} similaire---".format(s2.__name__, s1.__name__))
resp = simlation(s2, s1)
print(resp)
print("\n")

print("---couple {},{} similaire---".format(s2.__name__, random.__name__))
resp = simlation(s1, random)
print(resp)
print("\n")


print("---couple {},{} bisimilaire---".format(b1.__name__, b2.__name__))
resp = bisimulation(b1, b2)
print(resp)
print("\n")
print("---couple {},{} bisimilaire---".format(b2.__name__, b1.__name__))
resp = bisimulation(b2, b1)
print(resp)
print("\n")

print("---couple {},{} bisimilaire---".format(b2.__name__, random.__name__))
resp = bisimulation(b1, random)
print(resp)
print("\n")