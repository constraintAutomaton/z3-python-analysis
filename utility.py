
import inspect
import ast
import z3
import astor
from typing import List, Tuple, Any
import re

OPERATION = "[OPERATION]"


def collect_path_conditions(tree):
    paths = []

    def traverse_if_children(children, context, cond):
        old_paths = len(paths)
        condList = [cond]
        for child in children:
            operation = findOperator(child)
            if operation != "":
                condList += [operation]
            traverse(child, context + condList)
        if len(paths) == old_paths:
            paths.append(context + condList)

    def traverse(node, context):
        if isinstance(node, ast.If):
            cond = astor.to_source(node.test).strip()
            not_cond = "z3.Not" + cond

            traverse_if_children(node.body, context, cond)
            traverse_if_children(node.orelse, context, not_cond)

        else:
            for child in ast.iter_child_nodes(node):
                traverse(child, context)

    traverse(tree, [])
    return staticSingleAssignment(paths)


def findOperator(node):
    op = ""
    if not(isinstance(node, ast.If)):
        eq = factoryOperation(node)
        if eq != "":
            op += "({}),".format(eq)
    return op[:-1]


def factoryOperation(node):
    if isinstance(node, ast.AugAssign):
        var = node.target.id
        value = None
        sign = None
        if isinstance(node.op, ast.Sub):
            sign = "-"
        elif isinstance(node.op, ast.Div):
            sign = "/"
        elif isinstance(node.op, ast.MatMult):
            sign = "*"
        else:
            sign = "+"
        if hasattr(node.value, 'n'):
            value = node.value.n
        else:
            value = node.value.id
        return "{}=={}{}{}{}".format(var, var, sign, value, OPERATION)
    if isinstance(node, ast.Assign):
        value = None
        if hasattr(node.value, 'n'):
            value = node.value.n
        else:
            value = node.value.id
        var = node.targets[0].id
        return "{}=={}{}".format(var, value,OPERATION)

    return""


class Index:
    i = -1

    def reset():
        Index.i = -1

    def get():
        Index.i += 1
        return Index.i


def staticSingleAssignment(logicPath: List[List[str]]) -> Tuple[List[str], List[Any]]:

    res = []
    variables = []
    for path in logicPath:
        variable = {
            "x": 0,
            "y": 0,
            "k": 0,
        }
        currentX = "x"
        currentY = "y"
        currentK = "k"
        subRes = []
        for node in path:
            if OPERATION in node:
                if "x" in node:
                    variable["x"] += 1
                    currentX = "x"+str(variable["x"])
                if "y" in node:
                    variable["y"] += 1
                    currentY = "y"+str(variable["y"])
                if "k" in node:
                    variable["k"] += 1
                    currentK = "k"+str(variable["k"])
            newNode = remplaceAllOccurence(node, "x", currentX)
            newNode = remplaceAllOccurence(newNode, "y", currentY)
            newNode = remplaceAllOccurence(newNode, "k", currentK)
            subRes.append(newNode)
        res.append(subRes)
        variables.append(variable)

    return (res, getVariables(variables), variables)

def remplaceAllOccurence_raw(val: str, var: str, r: str) -> str:
    res = val
    if var in val:
        pos = [m.start() for m in re.finditer(var, val)]
        i = 0
        for v in pos:
            res = list(res)
            res[v+i] = r
            res = "".join(res)
            i += len(r)-1
    return res

def remplaceAllOccurence(val: str, var: str, r: str) -> str:
    res = val
    if var in val:
        pos = [m.start() for m in re.finditer(var, val)]
        i = 0
        for v in pos:
            res = list(res)
            res[v+i] = r
            res = "".join(res)
            i += len(r)-1
        if OPERATION in res:
            num = r[1:]
            num = int(num)
            num -= 1
            newR = "{}{}".format(var, num)
            if num != 0:
                res = res.replace(r, newR)
            else:
                newR = var
                res = res.replace(r, newR)
            res = res.replace(newR, r,1)
            res = res.replace(OPERATION, "")
    return res


def getVariables(variables) -> List[str]:
    x = []
    y = []
    k_ = []
    for var in variables:
        for k, v in var.items():
            if k == "x":
                x.append(v)
            if k == "y":
                y.append(v)
            if k == "k":
                k_.append(v)
    xmax = max(x)
    ymax = max(y)
    kmax = max(k_)
    var = ["x", "y", "k"]
    for i in range(1, xmax+1):
        var.append("x{}".format(i))
    for i in range(1, ymax+1):
        var.append("y{}".format(i))
    for i in range(1, kmax+1):
        var.append("k{}".format(i))
    return var


def generate_variable_statement(variables) -> str:
    variableStatement = ""
    for v in variables:
        variableStatement += "{}=z3.Int(\"{}\")\n".format(v, v)
    return variableStatement


def generate_z3_statement(variable: str, path_condition: List[str]) -> str:
    statement = variable
    for c in path_condition:
        statement += "s.add({})\n".format(c)
    return statement


def findMaxVariable(variables):
    resp = {
        "x": 0,
        "y": 0,
        "k": 0,
    }

    for v in variables:
        if resp["x"] < v["x"]:
            resp["x"] = v["x"]
        if resp["y"] < v["y"]:
            resp["y"] = v["y"]
        if resp["k"] < v["k"]:
            resp["k"] = v["k"]
    return resp

def deleteDeadCode(conditions: List[List[str]], variable_statement: List[str]) -> List[List[str]]:
    paths = []
    for path_condition in conditions:
        s = z3.Solver()
        statement = generate_z3_statement(variable_statement, path_condition)
        exec(statement)
        sat = s.check().__repr__()
        if sat == "sat":
            paths.append(path_condition)
    return paths
