from deadcode import *
from examples.test_generation.example_1 import *
from examples.test_generation.example_2 import *

deadCode = findDeadCode(example_1)
print("---{}---".format(example_1.__name__))
print("---dead code---")
for d in deadCode:
    print(d)

deadCode = findDeadCode(example_2)
print("---{}---".format(example_2.__name__))
print("---dead code---")
for d in deadCode:
    print(d)
