import inspect
import ast
import z3
import astor
from typing import List, Any, Tuple
from utility import *
from test_generator import *
# we suppose that the variable name are the same


def simlation(specification, func) -> str:
    path_condition_specification, variable_specification, _ = getPathCondition(
        specification)
    path_condition_func, variable_func, _ = getPathCondition(func)
    if len(path_condition_func) < len(path_condition_specification):
        return "the function don't have enough path conditions to simulate the specification"

    test_cases_specification = test_generator_from_condition(
        path_condition_specification, variable_specification, specification)
    test_cases_func = test_generator_from_condition(
        path_condition_func, variable_func, func)

    notPossible = []
    for i, test_cases in enumerate(test_cases_specification):
        if not(test_cases in test_cases_func):
            notPossible.append("behavior : {}".format(
                path_condition_specification[i]))
    if len(notPossible) == 0:
        return "the function can simulate the specification"
    return "the function is not able to simulate the specification because it can't have those behavior\n{}".format("\n".join(notPossible))


def bisimulation(specification, func) -> str:
    path_condition_specification, variable_specification, _ = getPathCondition(
        specification)
    path_condition_func, variable_func, _ = getPathCondition(func)
    if len(path_condition_func) != len(path_condition_specification):
        return "the functions doesn't have the same number of posible behavior"
        
    test_cases_specification = test_generator_from_condition(
        path_condition_specification, variable_specification, specification)
    test_cases_func = test_generator_from_condition(
        path_condition_func, variable_func, func)

    notPossible = []
    for i, test_cases in enumerate(test_cases_specification):
        if not(test_cases in test_cases_func):
            notPossible.append("behavior : {}".format(
                path_condition_specification[i]))
    if len(notPossible) == 0:
        return "the functions are bisimilar"
    return "the function are not bisimilar because the behavior those behavior of the first function are not met in the second\n{}".format("\n".join(notPossible))

def getPathCondition(func) -> Tuple[List[List[str]], Any, Any]:
    source_code = inspect.getsource(func)
    astTree = ast.parse(source_code)
    path_conditions, variables, variables_raw = collect_path_conditions(
        astTree)
    variable_statement = generate_variable_statement(variables)

    path_conditions = deleteDeadCode(path_conditions, variable_statement)
    return path_conditions, variables, variables_raw
